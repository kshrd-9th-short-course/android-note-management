package com.chanchhaya.note.mgt.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.chanchhaya.note.mgt.R;
import com.chanchhaya.note.mgt.adapter.NoteAdapter;
import com.chanchhaya.note.mgt.config.RoomDatabaseConfig;
import com.chanchhaya.note.mgt.entity.Note;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ListNoteActivity extends AppCompatActivity implements NoteAdapter.OnActionClick {

    private FloatingActionButton fabCreateNew;
    private RecyclerView recyclerView;
    private NoteAdapter noteAdapter;
    private LinearLayoutManager layoutManager;
    private List<Note> dataSet = new ArrayList<>();

    private RoomDatabaseConfig roomDatabaseConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_note);

        // invoke initViews function
        initViews();

        // init database
        roomDatabaseConfig = RoomDatabaseConfig.getInstance(this);

        // config note recycler view
        configRecyclerView();

        // invoke event fabCreateNewClicked
        onFabCreateNewClicked();


    }

    @Override
    protected void onStart() {
        super.onStart();

        reloadData();

    }

    private void initViews() {
        fabCreateNew = findViewById(R.id.fab_create_new);
        recyclerView = findViewById(R.id.rcv_note);
    }

    private void configRecyclerView() {
        dataSet = roomDatabaseConfig.getNoteDao().select();
        noteAdapter = new NoteAdapter(this, dataSet);
        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(noteAdapter);
    }

    private void reloadData() {
        dataSet = roomDatabaseConfig.getNoteDao().select();
        noteAdapter.setDataSet(dataSet);
        noteAdapter.notifyDataSetChanged();
    }

    private void onFabCreateNewClicked() {
        fabCreateNew.setOnClickListener(v -> {
            Intent intent = new Intent(this, CreateNoteActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void onEdit(int position) {
        Note noteForEdit = dataSet.get(position);
        Intent intent = new Intent(this, CreateNoteActivity.class);
        intent.putExtra("note", noteForEdit);
        startActivity(intent);
    }

    @Override
    public void onDelete(int position) {
        new AlertDialog.Builder(this)
                .setTitle("Delete operation!")
                .setPositiveButton("Delete!", (dialog, which) -> {
                    roomDatabaseConfig.getNoteDao().delete(dataSet.get(position));
                    dataSet.remove(position);
                    noteAdapter.notifyDataSetChanged();
                })
                .setCancelable(true)
                .show();
    }

}