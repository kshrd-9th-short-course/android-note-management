package com.chanchhaya.note.mgt.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.chanchhaya.note.mgt.config.Constants;
import com.chanchhaya.note.mgt.entity.Note;

import java.util.List;

@Dao
public interface NoteDao {

    // select note from database
    @Query("SELECT * FROM " + Constants.TABLE_NAME_NOTE + " ORDER BY note_id DESC")
    List<Note> select();

    // insert note into database
    @Insert
    void insert(Note note);

    // update existing note in database
    @Update
    void update(Note note);

    // delete note from database
    @Delete
    void delete(Note note);

}
