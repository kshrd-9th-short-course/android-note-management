package com.chanchhaya.note.mgt.config;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.chanchhaya.note.mgt.dao.NoteDao;
import com.chanchhaya.note.mgt.entity.Note;

@Database(entities = { Note.class }, version = 1)
public abstract class RoomDatabaseConfig extends RoomDatabase {

    public abstract NoteDao getNoteDao();

    private static RoomDatabaseConfig instance;

    public static synchronized RoomDatabaseConfig getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    RoomDatabaseConfig.class,
                    Constants.DB_NAME).allowMainThreadQueries().build();
        }
        return instance;
    }

}
