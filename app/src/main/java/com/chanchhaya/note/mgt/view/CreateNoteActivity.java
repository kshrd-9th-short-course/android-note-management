package com.chanchhaya.note.mgt.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.chanchhaya.note.mgt.R;
import com.chanchhaya.note.mgt.config.RoomDatabaseConfig;
import com.chanchhaya.note.mgt.entity.Note;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

public class CreateNoteActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatEditText editTitle;
    private AppCompatEditText editContent;
    private AppCompatButton btnCreate;
    private RoomDatabaseConfig roomDatabaseConfig;
    private boolean isUpdate = false;
    private Note note = new Note();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);

        // invoke init view function
        initViews();

        // initialize database
        roomDatabaseConfig = RoomDatabaseConfig.getInstance(this);

        if (getIntent().getSerializableExtra("note") != null) {
            isUpdate = true;
            btnCreate.setText(R.string.action_save);
            editTitle.requestFocus();
            note = (Note) getIntent().getSerializableExtra("note");
            editTitle.setText(note.getTitle());
            editContent.setText(note.getContent());
        }

        // register event
        btnCreate.setOnClickListener(this);

    }

    private void initViews() {
        editTitle = findViewById(R.id.edit_title);
        editContent = findViewById(R.id.edit_content);
        btnCreate = findViewById(R.id.button_create);
    }

    @Override
    public void onClick(View v) {
        String title = editTitle.getText().toString();
        String content = editContent.getText().toString();

        if (!title.isEmpty() && !content.isEmpty()) {

            note.setTitle(title);
            note.setContent(content);

            if (!isUpdate) {
                roomDatabaseConfig.getNoteDao().insert(note);
            } else {
                roomDatabaseConfig.getNoteDao().update(note);
            }

            finish();

        } else {
            Snackbar.make(editTitle, "Data cannot be empty!", BaseTransientBottomBar.LENGTH_SHORT).show();
            editTitle.requestFocus();
        }

    }

}