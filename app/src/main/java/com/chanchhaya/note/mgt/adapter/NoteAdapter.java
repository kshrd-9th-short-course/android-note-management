package com.chanchhaya.note.mgt.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.chanchhaya.note.mgt.R;
import com.chanchhaya.note.mgt.entity.Note;

import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {

    private List<Note> dataSet;
    private Context context;
    private LayoutInflater inflater;
    public OnActionClick onActionClick;

    public NoteAdapter(Context context, List<Note> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
        inflater = LayoutInflater.from(context);
        onActionClick = (OnActionClick) context;
    }

    public void setDataSet(List<Note> dataSet) {
        this.dataSet = dataSet;
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.note_item_layout, parent, false);
        return new NoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
        holder.textTitle.setText(dataSet.get(position).getTitle());
        holder.textContent.setText(dataSet.get(position).getContent());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    class NoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        AppCompatTextView textTitle;
        AppCompatTextView textContent;
        AppCompatImageButton btnEdit;
        AppCompatImageButton btnDelete;

        public NoteViewHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.text_title);
            textContent = itemView.findViewById(R.id.text_content);
            btnEdit = itemView.findViewById(R.id.button_edit);
            btnDelete = itemView.findViewById(R.id.button_delete);
            btnEdit.setOnClickListener(this);
            btnDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.button_edit) {
                onActionClick.onEdit(getAdapterPosition());
            } else if(v.getId() == R.id.button_delete) {
                onActionClick.onDelete(getAdapterPosition());
            }
        }

    }

    public interface OnActionClick {
        void onEdit(int position);
        void onDelete(int position);
    }

}
